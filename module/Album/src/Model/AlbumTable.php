<?php

namespace Album\Model;

use RuntimeExceptoin;
use Zend\Db\TableGateway\TableGatewayInterface;

/**
* 
*/
class AlbumTable
{
	private $tableGateway;
	
	function __construct(TableGatewayInterface $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}

	public function fetchAll() //ambil semua data
	{
		return $this->tableGateway->select();
	}

	public function getAlbum($id) //ambil data dengan parameter id
	{
		$id = (int) $id;
		$rowset = $this->tableGateway->select(['id'=>$id]);
		$row = $rowset->current();

		if(! $row){
			throw new RuntimeExceptoin(sprintf(
				'Data dengan parameter %d tidak ditemukan',
				$id
			));
		}
		return $row;

	}

	public function saveAlbum(Album $album) //simpan data. Meliputi insert dan update
	{
		$data = [
			'artist'	=> $album->artist,
			'title'		=> $album->title,
		];

		$id = (int) $album->id;

		if ($id === 0) {
			$this->tableGateway->insert($data);
			return;
		}

		if (! $this->getAlbum($id)) {
			throw new RuntimeException(sprintf(
				'Tidak bisa meng-update album dengan parameter %d, data tidak ditemukan', $id
			));
		}

		$this->tableGateway->delete(['id'=> (int) $id]);
	}

	public function deleteAlbum($id)
	{
		$this->tableGateway->delete(['id' => (int) $id]);
	}
}