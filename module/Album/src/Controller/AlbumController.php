<?php

namespace Album\Controller;

use Album\Model\AlbumTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Album\Form\AlbumForm;
use Album\Model\Album;

/**
* 
*/
class AlbumController extends AbstractActionController
{
	
	private $table;

	public function __construct(AlbumTable $table){
		$this->table = $table;
	}

	public function indexAction()
	{
        // echo "mane";
        return new ViewModel([
        	'albums' => $this->table->fetchAll(),
    	]);
	}

	public function addAction()
	{
		$form = new AlbumForm(); //inisialisasi class AlbumForm yg berisikan pendefinisian tipe form dan input
		$form->get('submit')->setValue('Add'); // ?? ini belum tau untuk apa? 
		// rupanya ini untuk menambahkan komponen text dengan nama "submit" yang memiliki isi "Add"
		// setelah disubmit akan terlempar variable "title=asd&artist=dddddd&submit=Add&id=true";

		$request = $this->getRequest(); // dapatkan semua lemparan request
		if(! $request->isPost()){ // jika metodenya bukan POST maka tampilkan form inputan baru
			return ['form' => $form];
		}

		// print_r($request->getPost()); //untuk melihat komponen yang dikirim ketika submit (post)
		// print_r($request->toString()); //untuk melihat detail seluruh request
		// die('hasil'.($request->isPost()) ? 'true' : 'false');
		
		$album = new Album(); //inisialisasi instance model class Album
		$form->setInputFilter($album->getInputFilter()); // ?? ini diambil dari mana yah?? padahal diClass AlbumForm tidak ada method setInputFilter() 
		$form->setData($request->getPost()); // ini juga

		// 2 fungsi diatas diambil dari class Form yang terdapat pada : vendor/zendframework/zend-form/src/Form.php
		// di class itu terdapat seluruh method yang diinisiasi ke variabel $form
		// class AlbumForm mewarisi class dari Form

		if(! $form->isValid()){ // jika isian form tidak valid maka kembalikan ke halaman form
			return ['form' => $form];
		}

		$album->exchangeArray($form->getData());
		$this->table->saveAlbum($album);
		return $this->redirect()->toRoute('album');
	}

	public function editAction()
	{
		$id = (int) $this->params()->fromRoute('id', 0);

		if(0 === $id){ // ane juga yah kondisinya jika $id 0
			return $this->redirect()->toRoute('album', ['action' => 'add']);
		}

		// baca album dengan spesifikasi id. 
		// sebuah eksepsi jika album tidak ditemukan, maka seharusnya 
		// akan me-redirect ke landing page

		try {
			$album = $this->table->getAlbum($id);
		} catch(\Exception $e) {
			return $this->redirect()->toRoute('album', ['action' => 'index']);
		}

		$form = new AlbumForm();
		$form->bind($album); // ini buat ngelempar ke form kayaknya.. 
		//karena di model Album sudah didefine data percis seperti pada struktur tampilan view.

		$form->get('submit')->setAttribute('value','Edit');

		$request = $this->getRequest();
		$viewData = ['id' => $id, 'form' => $form]; // nah pas 'form' => $form ini sudah berisi data hasil query

		// print_r($viewData);

		if(! $request->isPost()){
			return $viewData;
		}
		// die();

		$form->setInputFilter($album->getInputFilter()); // ??
		$form->setData($request->getPost()); // ??

		if(! $form->isValid()){ // ??
			return $viewData;
		}

		$this->table->saveAlbum($album); // simpan album

		// redirect ke list album
		return $this->redirect()->toRoute('album', ['action' => 'index']);

	}

	public function deleteAction()
	{
		$id = (int) $this->params()->fromRoute('id', 0);

		if(! $id) {
			return $this->redirect()->toRoute('album', ['action' => 'index']);
		}

		$request = $this->getRequest();
		if($request->isPost()){
			$del = $request->getPost('del','No');

			if($del == 'Yes') {
				$id = (int) $request->getPost('id');
				$this->table->deleteAlbum($id);
			}

			// redirect ke list album
			return $this->redirect()->toRoute('album');
		}

		return [
			'id' 	=> $id,
			'album'	=> $this->table->getAlbum($id),
		];
	}
/*
*/	
}
